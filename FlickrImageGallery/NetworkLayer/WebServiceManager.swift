//
//  WebServiceManager.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

typealias URLSessionCompletionBlock = (Data?, URLResponse?, Error?) -> Void

// This struct can be used throughout the app to create different data tasks
struct WebServiceManager {
    static let urlSession:URLSession = URLSession.shared
    static func dataForUrlRequest(_ request:URLRequest, completionHandler: @escaping URLSessionCompletionBlock) -> URLSessionDataTask {
        let task = urlSession.dataTask(with: request, completionHandler: { (data, response, error) in
            completionHandler(data, response, error)
        })
        task.resume()
        return task
    }
}
