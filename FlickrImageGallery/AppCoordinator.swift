//
//  AppCoordinator.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

protocol Coordinator {
    func start()
}

/// AppCoordinator for maintaining other coordinators that are used in the app
class AppCoordinator:Coordinator {
    
    let navigationController:UINavigationController!
    var coordinators:[Coordinator] = []
    
    init(withNavigationController navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        showGalleryList()
    }
    
    /// Pushes GalleryList coordinator in coordinator stack and started the coordinator
    func showGalleryList() {
        let galleryCoordinator = GalleryListCoordinator(withNavigationController: self.navigationController)
        galleryCoordinator.start()
        coordinators.append(galleryCoordinator)
    }
}
