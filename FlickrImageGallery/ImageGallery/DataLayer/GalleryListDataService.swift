//
//  GalleryListDataService.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

/// Protocol to define methods for classes that are supposed to provide data
protocol GalleryListDataService {
    func fetchGalleryList(handler:@escaping (GalleryList?)->())
    func galleryList(fromData data:Data) -> GalleryList?
}

extension GalleryListDataService {
    // Method implemention where JSONDecoder is used to convert Data to GalleryList
    // Can be overridden to provide other implementations
    func galleryList(fromData data:Data) -> GalleryList? {
        let jsonDecoder = JSONDecoder()
        do {
            return try jsonDecoder.decode(GalleryList.self, from: data)
        } catch(let error) {
            print(error.localizedDescription)
            return nil
        }
    }
}
