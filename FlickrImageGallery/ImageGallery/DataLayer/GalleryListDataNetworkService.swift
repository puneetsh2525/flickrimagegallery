//
//  GalleryListDataNetworkService.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

// This enum is used to return URLRequest for web service tasks in the app.
enum FlickrWebRequestBuilder {
    case publicGalleryFeed
    
    var urlRequest:URLRequest? {
        var request:URLRequest?
        switch self {
        case .publicGalleryFeed :
            let baseUrlString = "https://api.flickr.com/services/feeds/photos_public.gne"
            var urlComponents = URLComponents(string: baseUrlString)
            urlComponents?.queryItems = [
                URLQueryItem(name: "format", value: "json"),
                URLQueryItem(name: "nojsoncallback", value: "1")
            ]
            guard let url = urlComponents?.url else {
                return nil
            }
            request = URLRequest(url: url)
            request?.timeoutInterval = 120
            request?.httpMethod = "GET"
            return request
        }
    }
}

/// Conforms to GalleryListData protocol and get Gallery List data from network
class GalleryListDataNetworkService: GalleryListDataService {
    
    func fetchGalleryList(handler:@escaping (GalleryList?)->()) {
        guard let request = FlickrWebRequestBuilder.publicGalleryFeed.urlRequest else {
            handler(nil)
            return
        }
        let _ = WebServiceManager.dataForUrlRequest(request) {[weak self] (data, response, error) in
            guard let strongSelf = self else {return}
            guard let data = data else {
                handler(nil)
                return
            }
            handler(strongSelf.galleryList(fromData:data))
        }
    }
}
