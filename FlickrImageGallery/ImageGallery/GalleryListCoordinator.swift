//
//  GalleryListCoordinator.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation


/// Coordinator for List Page
class GalleryListCoordinator:Coordinator {
    let navigationController:UINavigationController!
    
    init(withNavigationController navigationController:UINavigationController) {
        self.navigationController = navigationController
    }
    
    
    /// Pushes instance of GalleryListVC in navigation stack
    func start() {
        let vm = GalleryListVM(dataService: GalleryListDataNetworkService())
        vm.delegate = self
        let vc = GalleryListVC.controller(viewModel: vm)
        self.navigationController .pushViewController(vc, animated: true)
    }
    
}

extension GalleryListCoordinator:GalleryListVMDelegate {
    /// Pushes instance of GalleryItemDetailVC in navigation stack
    func didSelect(item:GalleryListItemVM) {
        let vc = GalleryItemDetailVC.controller(for: item)
        self.navigationController.pushViewController(vc, animated: true)
    }
}
