//
//  GalleryList.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

// Model classes. These classes are modelled accrodig to response JSON from data layer
// Using Swift 4's Codable protocol to convert raw data to objects
struct GalleryList:Codable {
    let items:[GalleryListItem]
}

struct GalleryListItem:Codable {
    let title:String
    let link:String
    let dateTaken:String
    let description:String
    let published:String
    let author:String
    let authorId:String
    let tags:String
    let media:GalleryListItemMedia
    
    enum CodingKeys:String, CodingKey {
        case title = "title"
        case link = "link"
        case dateTaken = "date_taken"
        case description = "description"
        case published = "published"
        case authorId = "author_id"
        case author = "author"
        case tags = "tags"
        case media = "media"
    }
}

struct GalleryListItemMedia:Codable {
    let m:String
}
