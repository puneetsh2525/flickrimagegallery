//
//  GalleryItemDetailVC.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

class GalleryItemDetailVC: UIViewController, StoryboardIdentifiable {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var imageViewBehind: UIImageView!
    @IBOutlet weak var imageViewFront: UIImageView!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var publishedOn: UILabel!
    @IBOutlet weak var tags: UILabel!
    
    private var item:GalleryListItemVM!
    
    // Class func to return instance of GalleryItemDetailVC
    class func controller(for item:GalleryListItemVM) -> GalleryItemDetailVC {
        let vc:GalleryItemDetailVC = UIStoryboard(storyboard: .main).instantiateViewController()
        vc.item = item
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /// Configures view, using data points from GalleryListItemVM object
    func setUpView() {
        title = item.title
        from.text = item.author
        publishedOn.text = item.publishedDate
        imageViewFront.sd_setImage(with: item.imageUrl) {[weak self] (image, error, cacheType, url) in
            self?.imageViewBehind.image = image
        }
        tags.text = item.tags
    }
}
