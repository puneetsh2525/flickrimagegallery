//
//  GalleryListFlowLayout.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

protocol GalleryListFlowLayoutDelegate:class {
    func heightForPhoto(at indexPath:IndexPath) -> CGFloat
}

/// UICollectionViewFlowLayout subclass to provide custom flow layout logic 
class GalleryListFlowLayout: UICollectionViewFlowLayout {
    weak var delegate:GalleryListFlowLayoutDelegate?
    var numberOfColumns = 2
    var cellPadding: CGFloat = 10.0
    private var cache = [UICollectionViewLayoutAttributes]()
    private var contentHeight: CGFloat  = 0.0
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {return 0}
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { return }
        guard cache.isEmpty else{return}
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth )
        }
        var column = 0
        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
        
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0)
            let photoHeight = delegate?.heightForPhoto(at: indexPath) ?? 0
            let height = cellPadding + photoHeight
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            contentHeight = max(contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            column = column >= (numberOfColumns - 1) ? 0 : column+1
        }
    }
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes  in cache {
            if attributes.frame.intersects(rect ) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func invalidateLayout() {
        cache = [UICollectionViewLayoutAttributes]()
        super.invalidateLayout()
    }
}
