//
//  GalleryListItemCollectionViewCell.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

/// Custom UICollectionViewCell class to show data in list's collectionview
class GalleryListItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var title:UILabel!
    
    static let cellIdentifier = "GalleryListItemCollectionViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.layer.cornerRadius = 3.0; // corner radius to imageview
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
        self.title.text = nil
    }
}
