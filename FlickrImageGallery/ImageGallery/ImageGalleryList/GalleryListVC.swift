//
//  GalleryListVC.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import UIKit

class GalleryListVC: UIViewController, StoryboardIdentifiable {

    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            collectionView.delegate = self
        }
    }
    
    @IBOutlet weak var collectionViewLayout: GalleryListFlowLayout! {
        didSet{
            collectionViewLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
            collectionViewLayout.delegate = self
        }
    }
    
    private var viewModel: GalleryListVM!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    
    class func controller(viewModel:GalleryListVM) -> GalleryListVC {
        let vc:GalleryListVC = UIStoryboard(storyboard: .main).instantiateViewController()
        vc.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVM()
        setUpView()
    }
    
    func setUpView() {
        self.navigationItem.title = "Public Feed"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action:#selector(refreshClicked))
    }
    
    func setUpVM() {
        viewModel.updateLoadingStatus = {[weak self] (status) in
            DispatchQueue.main.async {
                guard let strongSelf = self else{return}
                switch status {
                case .loading:
                    strongSelf.view.bringSubview(toFront: strongSelf.loadingView)
                case .error(let errorString):
                    strongSelf.view.bringSubview(toFront: strongSelf.errorView)
                    self?.errorLabel.text = errorString
                case .result:
                    strongSelf.view.bringSubview(toFront: strongSelf.collectionView)
                }
            }
        }
        
        viewModel.reloadListViewClosure = {[weak self] in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
                self?.collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshClicked() {
        viewModel.fetchData()
    }

}

extension GalleryListVC:UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfListItems
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryListItemCollectionViewCell.cellIdentifier, for: indexPath) as? GalleryListItemCollectionViewCell else {
            fatalError("Cell not exists in storyboard")
        }
        
        guard let cellVM = viewModel.getListItemViewModel(at: indexPath) else {
            return cell
        }
        
        cell.title.text = cellVM.title
        cell.imageView.sd_setImage(with: cellVM.imageUrl, placeholderImage: nil, options: SDWebImageOptions.retryFailed) {[weak cell] (image, error, cache, url) in
            if cache == .none {
                cell?.imageView.alpha = 0.0
                UIView.animate(withDuration: 0.2, animations: {cell?.imageView.alpha = 1.0})
            }
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectItemAt(indexPath: indexPath)
    }

}

extension GalleryListVC:GalleryListFlowLayoutDelegate {
    func heightForPhoto(at indexPath:IndexPath) -> CGFloat {
        guard let cellVM = viewModel.getListItemViewModel(at: indexPath) else {
            return 0
        }
        return cellVM.size.height+40
    }
}

