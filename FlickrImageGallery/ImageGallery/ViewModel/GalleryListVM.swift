//
//  GalleryListVM.swift
//  FlickrImageGallery
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import Foundation

enum ListLoadStatus {
    case loading
    case error(String)
    case result
}

protocol GalleryListVMDelegate:class {
    func didSelect(item:GalleryListItemVM)
}

/// ViewModel class for GalleryList/Homepage
class GalleryListVM {
    let dataService:GalleryListDataService
    private var galleryList: GalleryList?
    weak var delegate:GalleryListVMDelegate?
    
    private var listItemViewModels: [GalleryListItemVM] = [GalleryListItemVM]() {
        didSet {
            self.reloadListViewClosure?()
            self.status = listItemViewModels.count > 0 ? .result : .error("Something went wrong. Please try again.")
        }
    }
    
    var status: ListLoadStatus = .result {
        didSet {
            self.updateLoadingStatus?(status)
        }
    }
    
    var numberOfListItems: Int {
        return listItemViewModels.count
    }
    
    var reloadListViewClosure: (()->())?
    var updateLoadingStatus: ((ListLoadStatus)->())?
    
    init(dataService:GalleryListDataService) {
        self.dataService = dataService
        fetchData()
    }
    
    func fetchData() {
        self.status = .loading
        dataService.fetchGalleryList {[weak self] (galleryList) in
            guard let strongSelf = self else {return}
            strongSelf.galleryList = galleryList
            strongSelf.listItemViewModels = strongSelf.createListItemViewModels(from: galleryList?.items) ?? [GalleryListItemVM]()
        }
    }
    
    func getListItemViewModel( at indexPath: IndexPath) -> GalleryListItemVM? {
        return listItemViewModels.count > indexPath.row ? listItemViewModels[indexPath.row] : nil
    }
    
    func createListItemViewModels(from items:[GalleryListItem]?) -> [GalleryListItemVM]? {
        guard let items = items else {
            return nil
        }
        return items.flatMap({GalleryListItemVM(from:$0)})
    }
    
    func didSelectItemAt(indexPath:IndexPath) {
        guard let item = getListItemViewModel(at: indexPath) else {
            return
        }
        delegate?.didSelect(item:item)
    }
}


struct GalleryListItemVM {
    let imageUrl:URL
    let title:String
    let tags:String
    let link:String
    let description:String
    var publishedDate:String?
    let author:String
    let size:CGSize
    
    init?(from item:GalleryListItem) {
        guard let url = URL(string:item.media.m) else {
            return nil
        }
        imageUrl = url
        title = item.title
        tags = item.tags.split(separator: " ").flatMap{String(format:"#\($0)")}.joined(separator:" ")
        link = item.link
        description = item.description
        publishedDate = item.published.split(separator: "T").flatMap{String($0)}.first
        author = item.author
        var size:CGSize = .zero
        size.width = GalleryListItemVM.widthHeight(from: description).0
        size.height = GalleryListItemVM.widthHeight(from: description).1
        self.size = size
    }
    
    static func widthHeight(from string:String) -> (CGFloat, CGFloat) {
        let desc = string.replacingOccurrences(of: "\"", with: "").split(separator: " ")
        let dict = desc.reduce([String:Double]()) { (dict, substring) -> [String:Double] in
            var dict = dict
            if substring.contains("width=") || substring.contains("height=") {
                let d = substring.split(separator: "=").map({String($0)})
                dict[d.first!] = Double(d.last!)
                return dict
            }
            return dict
        }
        return (CGFloat(dict["width"] ?? 0), CGFloat(dict["height"] ?? 0))
    }
}
