//
//  FlickrImageGalleryTests.swift
//  FlickrImageGalleryTests
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrImageGallery

class GalleryListDataServiceTests: XCTestCase {
    
    var networkService:GalleryListDataNetworkService?
    
    override func setUp() {
        super.setUp()
        networkService = GalleryListDataNetworkService()
    }
    
    override func tearDown() {
        networkService = nil
        super.tearDown()
    }
    
    func testNetworkServiceFetch() {
        let expect = XCTestExpectation(description: "network_json_fetch")
        
        networkService?.fetchGalleryList(handler: { (galleryList) in
            expect.fulfill()
            
            XCTAssertNotNil(galleryList, "gallery list should not be nil")
            XCTAssert(galleryList!.items.count > 0, "items count should be greater than zero")
            
            galleryList?.items.forEach({ (item) in
                XCTAssertNotNil(item.media.m, "image url should not ne nil")
            })
        })
        
        wait(for: [expect], timeout: 20)
    }
}
