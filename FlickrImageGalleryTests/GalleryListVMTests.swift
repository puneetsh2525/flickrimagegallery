//
//  GalleryListVMTests.swift
//  FlickrImageGalleryTests
//
//  Created by Puneet Sharma2 on 09/11/17.
//  Copyright © 2017 com.puneet.sh2525. All rights reserved.
//

import XCTest
@testable import FlickrImageGallery

extension ListLoadStatus: Equatable {}
public func ==(lhs: ListLoadStatus, rhs: ListLoadStatus) -> Bool {
    return String(stringInterpolationSegment: lhs) == String(stringInterpolationSegment: rhs)
}

class GalleryListVMTests:XCTestCase {
    var vm:GalleryListVM?
    var mockDataService:MockDataService?
    
    override func setUp() {
        super.setUp()
        mockDataService = MockDataService()
        vm = GalleryListVM(dataService: mockDataService!)
    }
    
    override func tearDown() {
        vm = nil
        mockDataService = nil
        super.tearDown()
    }
    
    func testVMFetchData() {
        vm!.fetchData()
        XCTAssert(mockDataService!.isFetchCalled)
    }
    
    func testCreateListItemViewModels() {
        let list = GalleryLocalJsonList.listFromJson()
        mockDataService!.list = list
        
        let expect = XCTestExpectation(description: "reload triggered")
        vm!.reloadListViewClosure = { () in
            expect.fulfill()
        }
        
        vm!.fetchData()
        mockDataService!.fetchSuccess()
        
        XCTAssertEqual( vm!.numberOfListItems, list!.items.count)
        
        wait(for: [expect], timeout: 1.0)
    }
    
    func testGalleryListItemVM() {
        let listItem = GalleryListItem(title: "Winter is coming!", link: "stark@westeros.com", dateTaken: "2017-01-01", description: "lone wolf dies but the pack survives", published: "2017-10-01", author: " George R.R. Martin ", authorId: "007", tags: "#khaleesi#R+L=J", media: GalleryListItemMedia(m: "https://nomediaavailable.png"))
        let listItemVM = GalleryListItemVM(from: listItem)!
        
        XCTAssertEqual(listItem.title, listItemVM.title)
        XCTAssertEqual(listItem.author, listItemVM.author)
        XCTAssertEqual(listItem.tags.split(separator: " ").flatMap{String(format:"#\($0)")}.joined(separator:" "), listItemVM.tags)
        XCTAssertEqual(listItem.link, listItemVM.link)
        XCTAssertEqual(listItem.description, listItemVM.description)
        XCTAssertEqual(listItem.published.split(separator: "T").flatMap{String($0)}.first, listItemVM.publishedDate)
        XCTAssertEqual(listItem.author, listItemVM.author)
        XCTAssertEqual(CGSize.zero, listItemVM.size)
    }
    
    func test_loading_when_fetching() {
        var status:ListLoadStatus = .loading
        let expect = XCTestExpectation(description: "Loading status updated")
        
        vm!.updateLoadingStatus = {(loadStatus) in
            status = loadStatus
            expect.fulfill()
        }
        
        vm!.fetchData()
        
        XCTAssertTrue(status == .loading)
        
        mockDataService!.fetchSuccess()
        XCTAssertFalse(status == .loading)
        
        wait(for: [expect], timeout: 1.0)
    }
    
    func testVMDelegate() {
        let dc = GalleryVMDelegateClass()
        vm!.delegate = dc
        let list = GalleryLocalJsonList.listFromJson()
        mockDataService!.list = list
        
        let expect = XCTestExpectation(description: "reload triggered")
        vm!.reloadListViewClosure = { () in
            expect.fulfill()
        }
        
        vm!.fetchData()
        mockDataService!.fetchSuccess()
        
        vm!.didSelectItemAt(indexPath: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(dc.isDelegateCalled)
    }
    
}

class GalleryVMDelegateClass:GalleryListVMDelegate {
    var isDelegateCalled:Bool = false
    func didSelect(item: GalleryListItemVM) {
        isDelegateCalled = true
    }
}

class MockDataService: GalleryListDataService {
    
    var isFetchCalled = false
    
    var list:GalleryList?
    var completeClosure: ((GalleryList?) -> ())?
    
    func fetchGalleryList(handler: @escaping (GalleryList?) -> ()) {
        isFetchCalled = true
        completeClosure = handler
    }
    
    func fetchSuccess() {
        completeClosure!(list)
    }
    
    func fetchFail() {
        completeClosure!(list)
    }
    
}

class GalleryLocalJsonList {
    class func listFromJson() -> GalleryList? {
        let json = "public_feed"
        guard let data = self.data(fromJson: json) else {
            return nil
        }
        return try? JSONDecoder().decode(GalleryList.self, from: data)
    }
    
    private class func data(fromJson json:String) -> Data? {
        guard let file = Bundle.main.url(forResource: json, withExtension: "json") else {
            return nil
        }
        return try? Data(contentsOf:file)
    }
}

