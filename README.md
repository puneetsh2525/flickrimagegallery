##FlickrImageGallery

This is a sample project to show images from flickr [public feed](https://www.flickr.com/services/feeds/docs/photos_public).

### Tools
* XCode 9.1/Swift 4 for development
* PostMan for confirming and debugging server response
* SourceTree for git

### Third Party Libraries
* [SDWebImage](https://github.com/rs/SDWebImage) to download and cache the image media

### Design
The app initial list page design is inspired from [pinterest](https://itunes.apple.com/us/app/pinterest/id429047995?mt=8) .
A refresh button is provided to refresh data from fickr public feed.
The design of the detail page is arbitrary and is shown just to understand the use of coordinators in app flow.


![](listpage.png) ![](detailpage.png)


### App Architecture
* [MVVM](https://medium.com/ios-os-x-development/ios-architecture-patterns-ecba4c38de52) with [Coordinators Redux](http://khanlou.com/2015/10/coordinators-redux/)
* MVVM is used to separate business logic from View 
* Coordinators are used to move app flow logic from ViewControllers to higher level objects (coordinators)

### Unit Test
* Test Cases are provided for ViewModel and NetworkDataServiceLayer	

